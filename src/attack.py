class ParsingException(Exception):
    pass


attack_enum = {
    0: "UDP",
    1: "Valve Source Engine",
    2: "DNS",
    3: "SYN Flood",
    4: "ACK Flood",
    5: "ACK STOMP",
    6: "GRE IP Flood",
    7: "GRE Ethernet Flood",
    8: "NOT IMPLEMENTED",
    9: "UDP Plain",
    10: "HTTP",
}

option_enum = {
    0: "Payload Size",
    1: "Random Payload",
    2: "IP Type of Service",
    3: "IP ident",
    4: "IP TTL",
    5: "IP don't fragment",
    6: "Source Port",
    7: "Dest Port",
    8: "DNS Domain Name",
    9: "Domain Name Header ID",
    10: "NOT IMPLEMENTED",
    11: "TCP URG Header Flag",
    12: "TCP ACK Header Flag",
    13: "TCP PSH Header Flag",
    15: "TCP RST Header Flag",
    14: "TCP SYN Header Flag",
    16: "TCP FIN Header Flag",
    17: "TCP Force Sequence #",
    18: "TCP Force ACK #",
    19: "GRE CONST IP",
    20: "Method HTTP Floos",
    21: "HTTP Post Data",
    22: "Path for HTTP Flood",
    23: "SSL/HTTPS",
    24: "Number of Sockets",
    25: "Source IP",
}


def parse_attack_command(msg_bytes):

    # reimplementation of attack_parse from mirai source code

    if len(msg_bytes) < 4:
        raise ParsingException(f"Error not enough message bytes while parsing duration. Expected 4, got "
                               f"{len(msg_bytes)}")
    duration = int.from_bytes(msg_bytes[:4], byteorder="big", signed=False)
    msg_bytes = msg_bytes[4:]
    if len(msg_bytes) < 1:
        raise ParsingException("Error not enough message bytes while parsing attack_vector. Expected 1, got 0")
    attack_vector = int.from_bytes(msg_bytes[:1], byteorder="big", signed=False)
    msg_bytes = msg_bytes[1:]
    if len(msg_bytes) < 1:
        raise ParsingException("Error not enough message bytes while parsing target_count. Expected 1, got 0")
    target_count = int.from_bytes(msg_bytes[:1], byteorder="big", signed=False)
    msg_bytes = msg_bytes[1:]
    if len(msg_bytes) < (5*target_count):
        raise ParsingException(f"Error not enough message bytes while parsing targets. Expected {5*target_count},"
                               f" got {len(msg_bytes)}")
    targets = []
    for i in range(target_count):
        ip = "{}.{}.{}.{}/{}".format(int.from_bytes(msg_bytes[:1], byteorder="big", signed=False),
                                     int.from_bytes(msg_bytes[1:2], byteorder="big", signed=False),
                                     int.from_bytes(msg_bytes[2:3], byteorder="big", signed=False),
                                     int.from_bytes(msg_bytes[3:4], byteorder="big", signed=False),
                                     int.from_bytes(msg_bytes[4:5], byteorder="big", signed=False))
        targets.append(ip)
        msg_bytes = msg_bytes[5:]
    if len(msg_bytes) < 1:
        raise ParsingException("Error not enough message bytes while parsing option_count. Expected 1, got 0")
    option_count = int.from_bytes(msg_bytes[:1], byteorder="big", signed=False)
    msg_bytes = msg_bytes[1:]
    if len(msg_bytes) < 3*option_count:
        raise ParsingException(f"Error not enough message bytes while parsing options. "
                               f"Expected at least 3, got {len(msg_bytes)}")
    options = []
    for i in range(option_count):
        option_key = int.from_bytes(msg_bytes[:1], byteorder="big", signed=False)
        option_size = int.from_bytes(msg_bytes[1:2], byteorder="big", signed=False)
        msg_bytes = msg_bytes[2:]
        if len(msg_bytes) < option_size:
            raise ParsingException(f"Error not enough message bytes while parsing options. Expected {option_size},"
                                   f" got {len(msg_bytes)}")
        option_value = msg_bytes[:option_size]
        msg_bytes = msg_bytes[option_size:]
        options.append((option_enum[option_key], option_value))
    return f"duration: {duration}, attack_vector: {attack_enum[attack_vector]}, target_count: {target_count}," \
           f" targets: {targets}, option_count: {option_count}, options: {options}"


if __name__ == "__main__":
    parse_attack_command(b'\x00\x00\x00\x05\n\x01\xaa\x14\x00\x0c \x00')
