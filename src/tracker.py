#!/usr/bin/env python3

import socket
from threading import Thread
import attack
import argparse
import ipaddress
import logging


class BotConnection(Thread):

    def __init__(self, host, port):
        super().__init__()
        self.port = port
        self.host = host

    def handle_connection(self, sock):
        try:
            sock.connect((self.host, self.port))
            sock.settimeout(60)
        except ConnectionRefusedError:
            logging.error(f"Connection refused on {self.host}:{self.port}!")
            return
        sock.sendall(b'\x00\x00\x00\x01')
        sock.sendall(b'\x0ctelnet.ARMv8')  # something has to be send here cnc apparently does not care what it is but seems to be a name of sorts
        sock.sendall(b'\x00\x00')
        logging.info(f"Connected to {self.host}:{self.port}!")
        buffer = b''
        while 1:
            try:
                msg = sock.recv(1024)
                buffer += msg
                if not msg:
                    logging.info(f"Disconnected from {self.host}:{self.port}")
                    break    # disconnect
                logging.debug(f"From {self.host}:{self.port} received: {msg} ({len(msg)} Bytes)")
                while len(buffer) >= 2:
                    length = int.from_bytes(buffer[:2], byteorder="big", signed=False)
                    if not length:
                        buffer = buffer[2:]    # discard ping answer
                    elif length <= len(buffer):
                        message = buffer[2:length]
                        buffer = buffer[length:]
                        try:
                            logging.info(f"{self.host}:{self.port} issued an attack:\n{attack.parse_attack_command(message)}")
                        except attack.ParsingException as e:
                            logging.error(f"Cannot parse attack command from {self.host}:{self.port}. Reason: {e}")
                    else:
                        break  # unfinished attack command

            except socket.timeout:
                sock.sendall(b'\x00\x00')
            except TimeoutError:
                logging.error(f"Connection with {self.host}:{self.port} timed out!")
                break

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            self.handle_connection(sock)


def _check_validity(ip):
    ip = ip.split(":")
    try:
        ipaddress.ip_address(ip[0])
    except ValueError:
        logging.warning(f"{ip} does not seem to be a valid ip address.")
        return False
    if len(ip) == 2:
        try:
            port = int(ip[1])
        except ValueError:
            logging.warning(f"Port '{ip[1]}' is NaN")
            return False
        if port > 65535 or port < 0:
            return False
        return True


def read_from_file(file, ingore_ip_format):
    ips = []
    with open(file, "r") as f:
        for line in f:
            if _check_validity(line.strip()) or ingore_ip_format:
                ips.append(line.strip())
    return ips


def main():
    parser = argparse.ArgumentParser(description="Tracker for the Mirai-Botnet and it's siblings")
    parser.add_argument("-ip",
                        help="IP addresses of cnc servers separated by space. Format: ip:port (e.g. 127.0.0.1:23)",
                        nargs='+')
    parser.add_argument("-logfile", help="Specify a logfile. logs to console per default.")

    parser.add_argument("-loglevel", help="Specify log level <normal | debug>", default="normal")

    parser.add_argument("-finput", help="Get ips from file rather than from parameter", metavar="file")

    parser.add_argument("-f", help="Don't check validity of IP address (useful for entering domain names)",
                        action="store_true")

    args = parser.parse_args()

    logLevel = logging.DEBUG if args.loglevel.lower() == "debug" else logging.INFO

    if args.logfile:
        logging.basicConfig(level=logLevel, format="{levelname}: ({asctime}): {message}", datefmt="%x - %X",
                            style="{", handlers=[logging.FileHandler(args.logfile), logging.StreamHandler()])
    else:
        logging.basicConfig(level=logLevel, format="{levelname}: ({asctime}): {message}", datefmt="%x - %X",
                            style="{")

    if args.ip is None and args.finput is None:
        parser.print_help()
        exit(0)

    ips = []
    if args.ip is not None:
        ips.extend(args.ip)
    if args.finput is not None:
        ips.extend(read_from_file(args.finput, args.f))

    logging.debug("Mirai-Tracker started")
    logging.debug(f"Received {len(ips)} IP args")
    connections = []
    for address in ips:
        if _check_validity(address) or args.f:
            ip, port = address.split(":")
            connections.append(BotConnection(ip, int(port)))
    for con in connections:
        con.start()
    logging.debug("All connections setup")


if __name__ == "__main__":
    main()
