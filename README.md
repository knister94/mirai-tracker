# Mirai-Tracker

This is a Mirai botnnet tracker that conects to a C&C server and poses as a bot to log commands.

## Requirements

* Python 3.6 or above

## Usage
`python3 tracker.py -ip <ip:port> ...`

If you want to log events to a file, you can use the `-logfile` parameter.

```
usage: tracker.py [-h] [-ip IP [IP ...]] [-logfile LOGFILE]
                  [-loglevel LOGLEVEL] [-finput file] [-f]

Tracker for the Mirai-Botnet and it's siblings

optional arguments:
  -h, --help          show this help message and exit
  -ip IP [IP ...]     IP addresses of cnc servers separated by space. Format:
                      ip:port (e.g. 127.0.0.1:23)
  -logfile LOGFILE    Specify a logfile. logs to console per default.
  -loglevel LOGLEVEL  Specify log level <normal | debug>
  -finput file        Get ips from file rather than from parameter
  -f                  Don't check validity of IP address (useful for entering
                      domain names)
```

## CNC Server
For testing purposes [mirai-docker](https://github.com/jsong302/mirai-docker) can be used as the CNC-Server.

**setup docker network**

`docker network create --subnet=172.20.0.0/16 botnet`

**Run dockered cnc**

`sudo docker run --net botnet --ip 172.20.0.2 -it cnc`

**Use telnet to connect to C&C server**

`telnet 172.20.0.2`

and login using `root`: `root` as credentials.

Using `?` a list of available commands or options can be displayed (context sensitive, just substiute `?` for something you don't know).